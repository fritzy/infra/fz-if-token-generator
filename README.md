# Fritzy infra token generator

Generate a token from a vault container.

## Environment variables

```yaml
VAULT_PASSWORD: vault password
VAULT_USERNAME: vault username
```
