#!/bin/bash

# Work in root project path
SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then 
    cd $SCRIPT_PATH
fi
cd ../..

stty -echo
echo Enter vault admin username :
read vault_username
echo Enter vault admin password :
read vault_password
stty echo
echo Token generation ...

docker run --rm -it \
  -e VAULT_USERNAME=$vault_username \
  -e VAULT_PASSWORD=$vault_password \
  -v $(pwd):/ansible \
  -v ~/.ssh/id_rsa_gitlab_ci:/root/.ansible/private_key \
  willhallonline/ansible:2.10-alpine-3.11 \
  ansible-playbook -i inventories/staging generate-token.yml
